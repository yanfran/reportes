<?php

?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Repse | Administrativo</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    

</head>

<body class="top-navigation">

    <div id="wrapper">



        <div id="page-wrapper" class="personalizado-bg">




            <div class="panel-header bg-primary-gradient">
                <div class="page-inner"> <!--py-5-->
                    <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                        <div class="ml-md-auto py-2 mb-4 menu">
                            <!-- <h2 class="text-white pb-2 fw-bold">Dashboard</h2> -->
                            <h4 class="text-white op-7 mb-2 d-inline p-2 "><i class="fa fa-file-text-o"></i><a style="color:white;" href="index.php"> Dashboard </a></h4>
                            <h4 class="text-white op-7 mb-2 d-inline p-2 "><i class="fa fa-sitemap"></i> <a style="color:white;" href="DatosFaltantesProveedor.php"> Proveedores </a></h4>                            
                            <h4 class="text-white op-7 mb-2 d-inline p-2 "><i class="fa fa-user"></i> Trabajadores</h4>
                        </div>
                        <div class="ml-md-auto py-2 py-md-0 mb-4">
                            <!-- <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search"> -->
                            <!-- <a href="#" class="btn btn-white btn-border btn-round mr-2">Manage</a>
                                <a href="#" class="btn btn-secondary btn-round">Add Customer</a> -->
                            </div>
                        </div>
                    </div>
                </div>


                <div class="wrapper wrapper-content animated fadeInRight">


                    <div class="row" style="margin-top: -13rem;">
                        <div class="col-lg-3">
                            <div class="widget style1 white-bg">
                                <div class="row">
                                    <div class="col-8 text-left">                                            
                                        <P class="titulo-card"> Proveedores en Incumplimiento</p>                 
                                            <h2 class="font-bold" id="totalProveedores1">0</h2>
                                        </div>
                                        <div class="col-4 text-right">
                                            <i id="icon" class="dripicons dripicons-bell fa-2x"></i>                                        
                                        </div>                                    
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="widget style1 white-bg">
                                    <div class="row">
                                        <div class="col-8 text-left">                                            
                                            <P class="titulo-card"> Documentos Faltantes </p>                                     
                                                <h2 class="font-bold" id="ContadorDocumentosFaltantesAdmin">0</h2>
                                            </div>
                                            <div class="col-4 text-right">
                                                <i id="icon" class="dripicons dripicons-upload fa-2x"></i>                                        
                                            </div>                                    
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="widget style1 white-bg">
                                        <div class="row">
                                            <div class ="col-8 text-left">                                            
                                                <P class="titulo-card"> Facturas por proveedores </p>                                 
                                                    <h2 class="font-bold" id="ContadorFacturasAdmin">0</h2>
                                                </div>
                                                <div class="col-4 text-right">
                                                    <i id="icon" class="dripicons dripicons-document fa-2x"></i>                                        
                                                </div>                                    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="widget style1 white-bg">
                                            <div class="row">
                                                <div class="col-8 text-left">                                            
                                                    <P class="titulo-card"> Listas negras 69 y 69B </p>                                    
                                                        <h2 class="font-bold" id="ConteoListasNegras">2</h2>
                                                    </div>
                                                    <div class="col-4 text-right">
                                                        <i id="icon" class="dripicons dripicons-warning fa-2x"></i>                                        
                                                    </div>                                    
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <br>
                                    <br>

                                    <div class="container-fluid">
                                        <div class="row mt-5">  
                                            <!-- Start Total Proyectos -->
                                            <div class="col-md-8 ">
                                                <div class="ibox ">
                                                    <div class="ibox-title header">
                                                        <h5>Análisis de Riesgos</h5>
                                                        <div class="float-right" style="margin-right: -80px;">
                                                            <div class="">
                                                                <button class="btn btn-xs btn-outline btn-success" type="button">Today</button>
                                                                <button class="btn btn-xs btn-outline btn-success" type="button" onClick="datosGradicaMes()">Monthly</button>
                                                                <button class="btn btn-xs btn-outline btn-success" type="button" onClick="datosGraficaAnios()">Annual</button>

                                            <!-- <button class="btn btn-xs btn-success dim" type="button">Today</button>
                                            <button class="btn btn-xs btn-success dim" type="button">Monthly</button>
                                            <button class="btn btn-xs btn-success dim" type="button">Annual</button> -->
                                            <!-- <button type="button" class="btn btn-xs btn-success">Today</button>
                                            <button type="button" class="btn btn-xs btn-success">Monthly</button>
                                            <button type="button" class="btn btn-xs btn-success">Annual</button> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="ibox-content body-content">
                                    <div class="row">
                                        <div class="col-lg-8">                                                                                                                                                                 
                                            <!-- <h4 class="mb-3">Bar chart </h4> -->                                                    
                                            <canvas id="barChart"></canvas>
                                            <canvas id="barChart2"></canvas>
                                            <div id="cargando" class="spiner-example" style="margin-top:60px;">              
                                                <div class="sk-spinner sk-spinner-wave">
                                                    <div class="sk-rect1"></div>
                                                    <div class="sk-rect2"></div>
                                                    <div class="sk-rect3"></div>
                                                    <div class="sk-rect4"></div>
                                                    <div class="sk-rect5"></div>
                                                </div>                                                
                                            </div>
                                            <!-- <div id="cargando" 
                                            style="margin-top:65px;">                                                
                                                    <center> <i class="fa fa-spinner fa-pulse fa-3x fa-fw" style="color:#007bff;"></i></center>                                                
                                                </div>                                                 -->
                                            </div>
                                            <div class="col-lg-4">
                                                <ul class="stat-list">
                                                    <li>
                                                        <div class="row">
                                                            <h2 class="no-margins" id="proyectos"></h2>
                                                            <h4 class="d-inline ml-3">Proyectos</h4>
                                                        </div>
                                                        <div class="stat-percent">3% <i class="fa fa-level-up text-success"></i></div>
                                                        <div class="progress progress-mini">
                                                            <div style="width: 30%;" class="progress-bar"></div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <h2 class="no-margins" id="montos"></h2>
                                                            <h4 class="d-inline ml-3">Montos</h4>
                                                        </div>
                                                        <div class="stat-percent">60% <i class="fa fa-level-down text-success"></i></div>
                                                        <div class="progress progress-mini">
                                                            <div style="width: 60%;" class="progress-bar"></div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <h2 class="no-margins" id="totalTrabajadores"></h2>
                                                            <h4 class="d-inline ml-3">Trabajadores</h4>
                                                        </div>
                                                        <div class="stat-percent">3% <i class="fa fa-bolt text-success"></i></div>
                                                        <div class="progress progress-mini">
                                                            <div style="width: 30%;" class="progress-bar"></div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <a href="DatosFaltantesProveedor.php" type="button" class="btn btn-sm btn-block btn-outline btn-rounded btn-success">Detalles</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>                                    
                                    </div>                                                                           
                                </div>
                            </div>
                            <!-- End Total Proyectos -->
                            <!-- Start Total Ordenes -->
                            <div class="col-md-4">
                                <div class="ibox ">
                                    <div class="ibox-title header">
                                        <h5 data-toggle="popover" data-placement="right" data-content="5 $100,00,00 Trabajadores 5">Total Ordenes</h5>                                            
                                    </div>
                                    <div class="ibox-content table-responsive body-content">                                            
                                        <table id="tablaTotalOrdenes" class="table table-hover no-margins">
                                            <thead>
                                                <tr>
                                                    <th>Proveedores</th>
                                                    <th width="10%">N* orden</th>
                                                    <th>Monto</th>
                                                    <th>Trabajadores</th>
                                                </tr>
                                            </thead>
                                            <tbody>                                        
                                            </tbody>
                                        </table>                                                    
                                    </div>                                                                           
                                </div>
                            </div>    
                            <!-- End Total Ordenes -->                            
                            
                        </div>
                    </div>

                    <div class="container-fluid">
                        <div class="row ">  
                            <!-- Start Total Proyectos -->
                            <div class="col-md-8 ">
                                <div class="ibox ">       
                                    <div class="ibox-title header">
                                        <h5>Top 5</h5>
                                        <div class="float-right" style="margin-right: -80px;">
                                            <div class="">                                        
                                                <button class="btn btn-xs btn-outline btn-success" type="button">First 5</button>
                                                <button class="btn btn-xs btn-outline btn-success" type="button">All</button>

                                            <!-- <button class="btn btn-xs btn-success dim" type="button">Today</button>
                                            <button class="btn btn-xs btn-success dim" type="button">Monthly</button>
                                            <button class="btn btn-xs btn-success dim" type="button">Annual</button> -->
                                            <!-- <button type="button" class="btn btn-xs btn-success">Today</button>
                                            <button type="button" class="btn btn-xs btn-success">Monthly</button>
                                            <button type="button" class="btn btn-xs btn-success">Annual</button> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="ibox-content body-content">
                                    <div class="row">
                                        <div class="col-lg-12">                                                                                                            
                                            <table id="top5" class="table table-hover no-margins">
                                                <thead>
                                                    <tr>
                                                        <th>Proveedores</th>
                                                        <th>Trabajadores</th>
                                                        <th>Monto del proyecto</th>                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>                                                                                                                                                   
                                        </div>                                        
                                    </div>                                    
                                </div>                                                                           
                            </div>
                        </div>
                        <!-- End Total Proyectos -->
                        <!-- Start Total Ordenes -->
                        <div class="col-md-4">
                            <div class="ibox ">
                                <div class="ibox-title header">
                                    <h5>Listas negra</h5>                                            
                                </div>
                                <div class="ibox-content table-responsive body-content">                                            
                                    <table id="listasNegras" class="table table-hover no-margins">
                                        <thead>
                                            <tr>
                                                <th>Validación</th>
                                                <th>Proveedores</th>
                                                <th></th>                                            
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- <tr>
                                                <td><small>0</small></td>
                                                <td>HUB - PORTAL</td>
                                                <td class="project-completion">
                                                    <small>Completion with: 48%</small>
                                                    <div class="progress progress-mini">
                                                        <div style="width: 48%;" class="progress-bar"></div>
                                                    </div>
                                                </td>                                            
                                            </tr>
                                            <tr>
                                                <td>0</span> </td>
                                                <td>DEMO - PSE - BIMBO - 20211209 - 1</td>
                                                <td class="project-completion">
                                                    <small>Completion with: 28%</small>
                                                    <div class="progress progress-mini">
                                                        <div style="width: 28%;" class="progress-bar"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><small>0</small> </td>
                                                <td>Demo - Autlan PSE</td>
                                                <td class="project-completion">
                                                    <small>Completion with: 73%</small>
                                                    <div class="progress progress-mini">
                                                        <div style="width: 73%;" class="progress-bar"></div>
                                                    </div>
                                                </td>                                     
                                            </tr>                                                                         -->
                                        </tbody>
                                    </table>                                                    
                                </div>                                                                           
                            </div>
                        </div>    
                        <!-- End Total Ordenes -->                            

                    </div>
                </div>

                



            </div>
        </div>        

    </div>
</div>



<!-- Mainly scripts -->
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>


<script src="js/table.js"></script>
<script src="js/functiones.js"></script>    

<script src="js/plugins/dataTables/datatables.min.js"></script>
<script src="js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

<!-- Tinycon -->
<script src="js/plugins/tinycon/tinycon.min.js"></script>

<script src="js/Chart.min.js"></script>    

<script>
    $(document).ready(function() {
        ContadorDocumentosFaltantesAdmin();
        ContadorFacturasAdmin();
        datosEstadisticas2(); 
        datosGraficaAnios();         
        listasNegrasP();
        DatosFaltantesProvedor();
        TotalOrdenes();
        top5();                              
    });
</script>


</body>

</html>
