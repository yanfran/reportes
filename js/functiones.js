// function tootip(){
//   // tooltipsetTimeout(()=>{
//     $('[data-toggle="tooltip"]').tooltip(); 
// //  },1000);
// }

// tootip();

function datosEstadisticas2(){    
    // var formData = new FormData(document.getElementById("formeditarperfil"));
    $.ajax({
      url: "apirest/MontoTotal.php",
      type: "post",
      dataType: "json",
    //   data: formData,
    cache: false,
    contentType: false,
    processData: false
  })

    .done(function(res){
      console.log(res)
      var totalFacturasIncilplimientos = res.data[0].TotalMonto.toFixed(2);
      $("#proyectos").html(res.data[0].TotalProyectos);
      $("#montos").html(totalFacturasIncilplimientos);
      $("#totalTrabajadores").html(res.data[0].TotalTrabajadores);
      $("#totalProveedores1").html(res.data[0].TotalProveedores);      
    // datosEstadisticas();
  });
}


  function datosGraficaAnios(){  
    $('#cargando').css("display","block");
    $('#barChart').css("display","none");
    $('#barChart2').css("display","none");
  // var formData = new FormData(document.getElementById("formeditarperfil"));
  $.ajax({
    url: "apirest/GraficaAnio.php",
    type: "post",
    dataType: "json",
  //   data: formData,
  cache: false,
  contentType: false,
  processData: false
})

  .done(function(res){  
    $('#barChart').html('');
    $('#cargando').css("display","none");
    $('#barChart').css("display","block");
    console.log(res);
    var ctx = document.getElementById( "barChart" );
    // var ToEstadistica = res.encadenado3.toFixed(2);
    // console.log(ToEstadistica);
  //    ctx.height = 200;
  var myChart = new Chart( ctx, {    
    type: 'bar',
    data: {
      labels: res.anios,
      datasets: [
      {
        label: "Proyectos",
        data: res.encadenado,
        borderColor: "rgba(0,123,255, 0.9)",
        borderWidth: "0",
        backgroundColor: "rgba(0,123,255, 0.5)"
      },
      {
        label: "Monto",
        data: res.encadenado3,
        borderColor: "rgba(0,81,116.09)",
        borderWidth: "0",
        backgroundColor: "rgba(0,81,116.07)"
      },
      {
        label: "Proveedores",
        data: res.encadenado2,
        borderColor: "rgba(0,0,0,0.09)",
        borderWidth: "0",
        backgroundColor: "rgba(0,0,0,0.07)",
        tension: 0.4,
        type: 'line'
      }

      ]
    },
    options: {
      scales: {
        yAxes: [ {
          ticks: {
            beginAtZero: true
          }
        } ]
      }
    }
  } );


});
}


function datosGradicaMes(){    
  $('#cargando').css("display","block");
  $('#barChart').css("display","none");
  // var formData = new FormData(document.getElementById("formeditarperfil"));
  $.ajax({
    url: "apirest/GraficaMes.php",
    type: "post",
    dataType: "json",
  //   data: formData,
  cache: false,
  contentType: false,
  processData: false
})

  .done(function(res){
    $('#barChart2').html('');
    $('#cargando').css("display","none");
    $('#barChart2').css("display","block");
    var ctx = document.getElementById( "barChart2" );
    

  //    ctx.height = 200;
  var myChart = new Chart( ctx, {    
    type: 'bar',
    data: {
      labels: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septembre','Octubre','Noviembre','Diciembre'],
      datasets: [
      {
        label: "Proyectos",
        data: res.encadenado,
        borderColor: "rgba(0,123,255, 0.9)",
        borderWidth: "0",
        backgroundColor: "rgba(0,123,255, 0.5)"
      },
      {
        label: "Monto",
        data: res.encadenado3,
        borderColor: "rgba(0,81,116.09)",
        borderWidth: "0",
        backgroundColor: "rgba(0,81,116.07)"
      },
      {
        label: "Proveedores",
        data: res.encadenado2,
        borderColor: "rgba(0,0,0,0.09)",
        borderWidth: "0",
        backgroundColor: "rgba(0,0,0,0.07)",
        tension: 0.4,
        type: 'line'
      }

      ]
    },
    options: {
      scales: {
        yAxes: [ {
          ticks: {
            beginAtZero: true
          }
        } ]
      }
    }
  } );


});
}


function ContadorDocumentosFaltantesAdmin(){    
    // var formData = new FormData(document.getElementById("formeditarperfil"));
    $.ajax({
      url: "apirest/ContadorDocumentosFaltantesAdmin.php",
      type: "post",
      dataType: "json",
    //   data: formData,
    cache: false,
    contentType: false,
    processData: false
  })

    .done(function(res){
      console.log(res)
      $("#ContadorDocumentosFaltantesAdmin").html(res.data[0].ContadorDocumentosFaltantesAdmin);
          
  });
}

function ContadorFacturasAdmin(){    
    // var formData = new FormData(document.getElementById("formeditarperfil"));
    $.ajax({
      url: "apirest/ContadorFacturasAdmin.php",
      type: "post",
      dataType: "json",
    //   data: formData,
    cache: false,
    contentType: false,
    processData: false
  })

    .done(function(res){
      console.log(res)
      $("#ContadorFacturasAdmin").html(res.data[0].ContadorFacturasAdmin);
          
  });
}


function ContadorDocumentosFaltantesClientes(){    
    // var formData = new FormData(document.getElementById("formeditarperfil"));
    $.ajax({
      url: "apirest/ContadorDocumentosFaltantesCliente.php",
      type: "post",
      dataType: "json",
    //   data: formData,
    cache: false,
    contentType: false,
    processData: false
  })

    .done(function(res){
      console.log(res)
      $("#DocumentosFaltantesClientes").html(res.data[0].ContadorDocumentosFaltantesCliente);
          
  });
}


function ContadorFacturasClientes(){    
    // var formData = new FormData(document.getElementById("formeditarperfil"));
    $.ajax({
      url: "apirest/ContadorFacturasClientes.php",
      type: "post",
      dataType: "json",
    //   data: formData,
    cache: false,
    contentType: false,
    processData: false
  })

    .done(function(res){
      console.log(res)
      $("#ContadorFacturasClientes").html(res.data[0].ContadorFacturasClientes);
          
  });
}



function ClistasNegras(){    
    // var formData = new FormData(document.getElementById("formeditarperfil"));
    $.ajax({
      url: "apirest/ContarListasNegras.php",
      type: "post",
      dataType: "json",
    //   data: formData,
    cache: false,
    contentType: false,
    processData: false
  })

    .done(function(res){
      console.log(res)
      $("#ConteoListasNegras").html(res.data[0].ContadorListasNegras);
          
  });
}


function clickCelda(celda, tipo){
  if(tipo==0){        
    $(".celda"+celda).css("display", "table-column");
    $("#botonV"+celda).css("display", "table-cell");
    $("#botonM"+celda).css("display", "none");
    console.log("cerrando");
  }else{
    $(".celda"+celda).css("display", "table-cell");
    $("#botonV"+celda).css("display", "none");
    $("#botonM"+celda).css("display", "table-cell");
    console.log("abriendo");
  }

}

function clickCelda2(celda, tipo){
  if(tipo==0){    
    // $(".Opciones").css("display", "none");
    $(".celdaC"+celda).css("display", "table-column");
    $(".text-center celdaC"+celda).css("display", "table-column");
    $("#botonVC"+celda).css("display", "table-cell");
    $("#botonMC"+celda).css("display", "none");
    console.log("cerrando");
  }else{
    // $(".Opciones").css("display", "block");
    $(".celdaC"+celda).css("display", "table-cell");


    $("#botonVC"+celda).css("display", "none");
    $("#botonMC"+celda).css("display", "table-cell");
    console.log("abriendo");
  }

}

function clickCelda3(celda, tipo){
  if(tipo==0){
    $(".celdaE"+celda).css("display", "table-column");
    $(".CeldaMes"+celda).css("display", "table-column");
    $("#botonVE"+celda).css("display", "table-cell");
    $("#botonME"+celda).css("display", "none");
    console.log("cerrando");
  }else{
    $(".celdaE"+celda).css("display", "table-cell");
    $("#botonVE"+celda).css("display", "none");
    $("#botonME"+celda).css("display", "table-cell");
    console.log("abriendo");
  }

}
function clickCeldaAnio(celda, tipo){
  console.log(celda);
  if(tipo==0){

    $(".A"+celda).each(function(){

      var clases=$(this)[0].classList;
      if($(this)[0].classList[1] != "A"+celda){
        var visibilidad=$("."+$(this)[0].classList[1]).is(":visible");
        if(visibilidad==true){
           $(this).css("display", "table-column");
        }
      }

    });

    $("#botonVANIOS"+celda).css("display", "table-cell");
    $("#botonOCULTANIOS"+celda).css("display", "none");
/*    $(".celdaAniosM"+celda).css("display", "none");
    $("#botonVANIOS"+celda).css("display", "table-cell");
    $("#botonOCULTANIOS"+celda).css("display", "none");
    console.log("cerrando");*/
  }else{
    // $(".celdaAniosM"+celda).css("display", "table-cell");
    $(".A"+celda).each(function(){

      var clases=$(this)[0].classList;
      if($(this)[0].classList[1] != "A"+celda){
        var visibilidad=$("."+$(this)[0].classList[1]).is(":visible");
        if(visibilidad==true){
          $(this).css("display", "table-cell");
        }
      }
    });

    $("#botonVANIOS"+celda).css("display", "none");
    $("#botonOCULTANIOS"+celda).css("display", "table-cell");
    // console.log("abriendo");
  }

}
